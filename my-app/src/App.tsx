import React, { useState } from 'react'
import './App.css'

function App() {

  const [firstname, setFirstname] = useState<string>('')
  const [lastname, setLastname] = useState<string>('')
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [users, setUsers] = useState([])

  const getUsers = async (e: any) => {
    e.preventDefault()
    const res = await fetch('http://localhost:3001/user/all', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })

    const result = await res.json()
    setUsers(result)
    console.log('fetching result: ', result)
  }

  const onSubmit = async (e: any) => {
    e.preventDefault()
    const res = await fetch('http://localhost:3001/user/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({firstname: firstname, lastname: lastname, email: email, password: password})
    })

    const result = await res.json()
    console.log('result response', result)
    //console.log('submit value', input)
  }

  return (
    <div className="container">
      <div className="row mt-4">
        <div className="col-12 col-md-6 offset-md-3">
          <h1 className="my-2 text-center">🐳</h1>
          <h2 className="my-4 text-center">Docker Workshop</h2>

          <form onSubmit={onSubmit}>
            <div className="form-group">
              <label>Firstname</label>
              <input
                type="text"
                className="form-control"
                onChange={e => setFirstname(e.target.value)}
                placeholder="Enter firstname"
                name="firstname"
                required
              />
            </div>
            <div className="form-group">
              <label>Lastname</label>
              <input
                type="text"
                className="form-control"
                onChange={e => setLastname(e.target.value)}
                placeholder="Enter lastname"
                name="lastname"
                required
              />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                className="form-control"
                aria-describedby="emailHelp"
                onChange={e => setEmail(e.target.value)}
                placeholder="Enter email"
                name="email"
                required
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                onChange={e => setPassword(e.target.value)}
                placeholder="Enter password"
                name="password"
                required
              />
            </div>
            <div className="col text-center">
              <button type="submit" className="btn btn-primary">
                Create
              </button>{' '}
              <button onClick={getUsers} className="btn btn-success">
                Get Users
              </button>
            </div>
          </form>
          <br/>
          
          <table className="table">
          {Object.keys(users).length > 0?
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Firstname</th>
              <th scope="col">Lastname</th>
            </tr>
          </thead>: ''}
          <tbody>
          {Object.values(users).map((item: any, i:number) => (
              <tr key={i}>
                <td>{i}</td>
                <td>{item.firstname}</td>
                <td>{item.lastname}</td>
              </tr>
          ))}
          </tbody>
          </table>
          
        </div>
      </div>
    </div>
  );
}

export default App
