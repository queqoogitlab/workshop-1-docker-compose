# workshop-1-docker-compose 🐳

## Pull specific image that i had pushed before
* docker pull queqoodk/docker-react-ts-ui
* Or another way build docker image from my-app 

## Run
* docker compose up -d
* docker compose down

## Add on Docker Compose
I have been mounting local folder name "data" to store database volume and init database with .sql in folder name "schema"

## Todo ...
* somethong with socket.io
* yaml for ci
* how to connect the cluster
