const express = require('express');
const dotenv = require('dotenv')

dotenv.config({path: '.env-local'});

const PORT = process.env.PORT || '3001';

const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded({extended:false}));

// route
app.get('/', (req, res) => {
    res.status(200).send('hello ... yoyo')
})

const userRouter = require('./routes/user');
app.use('/user', userRouter);

// start listening
app.listen(PORT, () => {
    console.log('lISTEN FOR RES ON PORT: ', PORT)
})