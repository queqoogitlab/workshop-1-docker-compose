const express = require('express');
const pool = require('../helpers/database');
const router = express.Router();

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

router.get('/all', async function(req, res){
    try {
        const sqlQuery = 'SELECT id, firstname, lastname, email, password, created_at FROM user';
        const rows = await pool.query(sqlQuery);
        res.status(200).json(rows);
    } catch (error) {
        res.status(400).send(error.message)
    }

    //res.status(200).json({id:req.params.id})
})

router.get('/:id', async function(req, res){
    try {
        const sqlQuery = 'SELECT id, email, firstname, lastname, password, created_at FROM user WHERE id=?';
        const rows = await pool.query(sqlQuery, req.params.id);
        res.status(200).json(rows);
    } catch (error) {
        res.status(400).send(error.message)
    }

    res.status(200).json({id:req.params.id})
})

router.post('/register', async function(req, res) {
    try {
        const {firstname, lastname, email, password} = req.body;

        const sqlQuery = 'INSERT INTO user (firstname, lastname, email, password) VALUES (?,?,?,?)';
        const result = await pool.query(sqlQuery, [firstname, lastname, email, password]);
        res.status(200).json(result);
        //res.status(200).send('ok add user: ', firstname)

    } catch (error) {
        res.status(400).send(error.message)

    }
})

module.exports = router;