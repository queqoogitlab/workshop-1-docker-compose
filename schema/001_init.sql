create database user_demo;
create table user_demo.user (id INT(10) NOT NULL AUTO_INCREMENT, firstname VARCHAR(100), lastname VARCHAR(100), email VARCHAR(100), password VARCHAR(100), created_at DATETIME, PRIMARY KEY (id));
insert into user_demo.user (firstname, lastname, email, password) values ('dojo', 'bobo', 'test1@user.com', '123456');